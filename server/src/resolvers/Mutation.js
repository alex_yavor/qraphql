function postMessage(parent, args, context, info) {
  return context.prisma.createMessage({
    body: args.body,
    likes: 0,
    dislikes: 0
  });
}
async function likeMessage(parent, args, context, info) {
  const message = await context.prisma.message({ id: args.messageId })
  return context.prisma.updateMessage({ data: { likes: message.likes + 1 }, where: { id: args.messageId } });
}

async function dislikeMessage(parent, args, context, info) {
  const message = await context.prisma.message({ id: args.messageId })
  return context.prisma.updateMessage({ data: { dislikes: message.dislikes + 1 }, where: { id: args.messageId } });
}

async function replyToMessage(parent, args, context, info) {
  const messageExists = await context.prisma.$exists.message({
    id: args.messageId
  });

  if (!messageExists) {
    throw new Error(`message with ID ${args.messageId} does not exist`);
  }

  return context.prisma.createMessage({
    body: args.body,
    reply: args.messageId,
    likes: 0,
    dislikes: 0,
  });
}

module.exports = {
  postMessage,
  replyToMessage,
  likeMessage,
  dislikeMessage
}