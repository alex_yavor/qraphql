function replyTo(parent, args, context) {
    if (!parent.reply) return null;
    return context.prisma.message({
        id: parent.reply
    });
}

module.exports = {
    replyTo
}