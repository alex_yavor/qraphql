import gql from 'graphql-tag';

export const MESSAGE_QUERY = gql`
  query messageQuery($orderBy: MessageOrderByInput,$filter:String,$last:Int) {
    messages(orderBy: $orderBy,filter:$filter,last:$last) {
      count
     messageList {
        id
        body
        likes
        dislikes
        replyTo {
          id
          body
          likes
          dislikes
        }
      }
    }
  }
`;

export const POST_MESSAGE_MUTATION = gql`
  mutation PostMutation($body: String!) {
    postMessage(body: $body) {
        id
        body
        likes
        dislikes
        replyTo {
          id
          body
          likes
          dislikes
        }
      }
  }
`;

export const LIKE_MESSAGE_MUTATION = gql`
  mutation PostMutation($messageId:ID!) {
    likeMessage(messageId: $messageId) {
        id
        body
        likes
        dislikes
        replyTo {
          id
          body
          likes
          dislikes
        }
      }
  }
`;

export const DISLIKE_MESSAGE_MUTATION = gql`
  mutation PostMutation($messageId:ID!) {
    dislikeMessage(messageId: $messageId) {
        id
        body
        likes
        dislikes
        replyTo {
          id
          body
          likes
          dislikes
        }
      }
  }
`;

export const REPLY_TO_MESSAGE_MUTATION = gql`
  mutation PostMutation($messageId:ID!,$body: String!) {
    replyToMessage(messageId: $messageId,body: $body,) {
        id
        body
        likes
        dislikes
        replyTo {
          id
          body
          likes
          dislikes
        }
      }
  }
`;

export const NEW_MESSAGE_SUBSCRIPTION = gql`
  subscription {
    newMessage {
        id
        body
        likes
        dislikes
        replyTo {
          id
          body
          likes
          dislikes
        }
      }
  }
`;

export const REACT_MESSAGE_SUBSCRIPTION = gql`
  subscription {
    reactMessage {
        id
        body
        likes
        dislikes
        replyTo {
          id
          body
          likes
          dislikes
        }
      }
  }
`;