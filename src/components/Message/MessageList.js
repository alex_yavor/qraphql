import React, { useState, useRef } from 'react';
import { Query } from 'react-apollo';
import MessageItem from './MessageItem';
import { MESSAGE_QUERY, NEW_MESSAGE_SUBSCRIPTION, REACT_MESSAGE_SUBSCRIPTION } from '../../queries';

const MessageList = props => {
    let list = useRef(null);
    let [filter, setFilter] = useState(null);
    let [typedFilter, setTypedFilter] = useState("");
    let [last, setSkip] = useState(5);
    let [orderBy, setOrder] = useState('createdAt_ASC');
    const orders = [
        'createdAt_DESC',
        'createdAt_ASC',
        'likes_ASC',
        'likes_DESC',
        'dislikes_ASC',
        'dislikes_DESC']
   

    const scrollToBottom = () => {
        setTimeout(() => {
            const scrollHeight = list.current.scrollHeight;
            const height = list.current.clientHeight;
            const maxScrollTop = scrollHeight - height;
            list.current.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
        }, 100)
    }

    const _subscribeToReactMessages = subscribeToMore => {
        subscribeToMore({
            document: REACT_MESSAGE_SUBSCRIPTION,
            updateQuery: (prev, { subscriptionData }) => {
                if (!subscriptionData.data) return prev;
                const { reactMessage } = subscriptionData.data;
                const messageList = prev.messages.messageList.map(message => message.id === reactMessage.id ? reactMessage : message);
                return {
                    ...prev, messages: {
                        messageList: [...messageList],
                        count: prev.messages.messageList.length,
                        __typename: prev.messages.__typename
                    }
                };
            }
        });
    };

    const _subscribeToNewProducts = subscribeToMore => {
        subscribeToMore({
            document: NEW_MESSAGE_SUBSCRIPTION,
            updateQuery: (prev, { subscriptionData }) => {
                if (!subscriptionData.data) return prev;

                const { newMessage } = subscriptionData.data;
                const exists = prev.messages.messageList.find(({ id }) => id === newMessage.id);
                scrollToBottom()
                if (exists) {
                    prev.messages.messageList.pop();
                }
                return {
                    ...prev, messages: {
                        messageList: [...prev.messages.messageList, newMessage],
                        count: prev.messages.messageList.length + 1,
                        __typename: prev.messages.__typename
                    }
                };
            }
        });
    };

    return (
        <Query query={MESSAGE_QUERY} variables={{ orderBy, filter, last }}>
            {({ loading, error, data, subscribeToMore }) => {
                if (loading) return <div>Loading...</div>;
                if (error) return <div>Fetch error</div>;
                _subscribeToNewProducts(subscribeToMore);
                _subscribeToReactMessages(subscribeToMore);
                const { count, messages: { messageList } } = data;
                return (
                    <div>
                        <div
                            ref={list}
                            className="message-list">
                            {(!count || last < count) && <button onClick={() => setSkip(last + 5)}>Load More</button>}
                            {messageList.map(item => {
                                return <MessageItem key={item.id} {...item} />
                            })}

                        </div>
                        <div> Sorted by:
                        <select value={orderBy} onChange={e => setOrder(e.target.value)}>
                                {orders.map(order => {
                                    return <option key={order} value={order}>{order}</option>
                                })}
                            </select>
                        </div>
                        <div>
                            <input
                                placeholder='filter'
                                value={typedFilter}
                                onChange={e => setTypedFilter(e.target.value)} />
                            <button onClick={() => setFilter(typedFilter)}>Filter</button> </div>
                    </div>
                );
            }}
        </Query>
    );
};

export default MessageList;