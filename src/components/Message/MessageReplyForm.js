import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { MESSAGE_QUERY, REPLY_TO_MESSAGE_MUTATION } from '../../queries';

const MessageReplyForm = ({ messageId, toggleForm }) => {
    const [body, setBody] = useState('');

    const _updateStoreAfterAddingReview = (store, newReview, productId) => {
        const orderBy = 'createdAt_ASC';
        const data = store.readQuery({
            query: MESSAGE_QUERY,
            variables: {
                orderBy
            }
        });
        const reviewedProduct = data.products.productList.find(
            item => item.id === productId
        );
        reviewedProduct.reviews.push(newReview);
        store.writeQuery({ query: MESSAGE_QUERY, data });
        toggleForm(false);
    };
    const mutate = postMutation => {
        if (body) {
            postMutation();
            setBody('');
            toggleForm();
        }
    }
    return (
        <div className="form-wrapper">
            <div className="input-wrapper">
                <textarea
                    onChange={e => setBody(e.target.value)}
                    placeholder="Message text"
                    autoFocus
                    value={body}
                    cols="25"
                />
            </div>
            <Mutation
                mutation={REPLY_TO_MESSAGE_MUTATION}
                variables={{ body, messageId }}
            // update={(store, { data: { postReview } }) => {
            //   _updateStoreAfterAddingReview(store, postReview, productId)
            // }}
            >
                {postMutation =>
                    <button onClick={() => mutate(postMutation)}>Reply</button>
                }
            </Mutation>
        </div>
    );
};

export default MessageReplyForm;