import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { LIKE_MESSAGE_MUTATION, DISLIKE_MESSAGE_MUTATION, MESSAGE_QUERY } from '../../queries';
import MessageReplyForm from './MessageReplyForm';

const MessageItem = ({ id, body, likes, dislikes, replyTo,myMessage }) => {
  const [showReplyForm, toggleForm] = useState(false);
  const _updateStoreAfterLikeMessage = (store, newMessage, id) => {
    const orderBy = 'createdAt_ASC';
    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: {
        orderBy,
        last:5,
        filter:""
      }
    });
    // data.messages.messageList = data.messages.messageList.map(message => {
    //   return message.id === id ? newMessage : message
    // })
    // data.messages.messageList.push(newMessage);
    store.writeQuery({
      query: MESSAGE_QUERY,
      data,
    });
  };

  return (
    <div
      className={myMessage ? "message right" : "message"}
      key={id}>
      <div>#{id}</div>
      <div className="message-text">{body}</div>
      <Mutation
        mutation={LIKE_MESSAGE_MUTATION}
        variables={{ messageId: id }}
      // update={(store, { data: { postMessage } }) => {
      //   _updateStoreAfterLikeMessage(store, postMessage, id);
      // }}
      >
        {postMutation =>
          <span
            onClick={() => postMutation()}
            className="hover">
            <img
              onClick={() => postMutation()}
              className="icon hover"
              src='like.svg'
              alt="Like" />
            {likes}
          </span>
        }
      </Mutation>

      <Mutation
        mutation={DISLIKE_MESSAGE_MUTATION}
        variables={{ messageId: id }}
      // update={(store, { data: { postMessage } }) => {
      //   _updateStoreAfterLikeMessage(store, postMessage, id);
      // }}
      >
        {postMutation =>
          <span
            onClick={() => postMutation()}
            className="hover">
            <img
              onClick={() => postMutation()}
              className="icon hover"
              src='dislike.svg'
              alt="Dislike" />
            {dislikes}
          </span>
        }
      </Mutation>

      {replyTo ?
        <div
          className={myMessage ? "message right" : "message"}
          key={replyTo.id}>
          Replied to:
          <div>#{replyTo.id}</div>
          <div className="message-text">{replyTo.body}</div>
        </div> :
        <span
          className="edit"
          onClick={() => toggleForm(!showReplyForm)}
        >
          Reply</span>
      }
      {showReplyForm && <MessageReplyForm
        messageId={id}
        toggleForm={toggleForm}
      />}
    </div>
  );
};

export default MessageItem;