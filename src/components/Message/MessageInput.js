import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { POST_MESSAGE_MUTATION, MESSAGE_QUERY } from '../../queries';

const MessageForm = props => {
  const [body, setBody] = useState('');

  const _updateStoreAfterAddingProduct = (store, newMessage) => {
    const orderBy = 'createdAt_ASC';
    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: {
        orderBy,
        last:5,
        filter:""
      }
    });
    // data.messages.messageList.push(newMessage);
    store.writeQuery({
      query: MESSAGE_QUERY,
      data,
    });
  };

  const mutate = (postMutation) => {
    if (body) {
      setBody("");
      postMutation();
    }
  }

  return (
    <div className="form-wrapper">

      <input type="text" placeholder="Message" value={body} onChange={e => setBody(e.target.value)} />


      <Mutation
        mutation={POST_MESSAGE_MUTATION}
        variables={{ body }}
        //it works without it
        // update={(store, { data: { postMessage } }) => {
        //   _updateStoreAfterAddingProduct(store, postMessage);
        // }}
      >
        {postMutation =>
          <button className="send-message-button" onClick={() => mutate(postMutation)}>Send</button>
        }
      </Mutation>
    </div>
  );
};

export default MessageForm;