import React from 'react';
import MessageList from './Message/MessageList';
import './App.scss';
import MessageForm from './Message/MessageInput';

function App() {
  return (
    <div className="App">
      <MessageList></MessageList>
      <MessageForm></MessageForm>
    </div>
  );
}

export default App;
